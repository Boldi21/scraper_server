// Imports
import {Server} from "./server/Server";
import {Fetcher} from "./server/Scraper/Fetcher";
import {FileHandler} from "./server/Utilities/FileHandler";
import {Parser} from "./server/Scraper/Parser";

// Instantiate server
var server = new Server();

// Initialize router and routes
server.initializeRouters();

// Start server
server.start();

// TEST
/*****
let fetcher = new Fetcher(["http://www.pcgarage.ro/sisteme-pc-garage/"]);

while (!fetcher.fetcherEmpty()) {
    try {
        fetcher.fetchNext(handleParsing);
    } catch (e) {
        console.log(e.message);
    }
}

function handleParsing(err, data) {

    if (err) {
        console.log("Error: " + err.message);
    } else {
        let fh = new FileHandler('pcgarage.json', './Files/templates/', ['read']);
        let template = fh.read('JSON');
        let parser = new Parser(data, template);
        parser.parse();
    }

}

**/