import {Meta} from "../Meta";
/**
 * Used to dynamically create objects with different container classes
 * in runtime
 */
export class ContainerFactory {

    /**
     * Initializes factory dictionary
     */
    constructor() {
    }

    /**
     * Builds object depending on className
     * @param className
     * @param params
     */
    build(className: string, params: Object) {
        // Return new class of type className, if not found
        // return object with class of set default class
        return className in Meta.types ? new Meta.types[className](params) : new Meta.types['default'](params);
    }

}

