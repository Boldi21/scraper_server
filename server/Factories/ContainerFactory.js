"use strict";
var Meta_1 = require("../Meta");
/**
 * Used to dynamically create objects with different container classes
 * in runtime
 */
var ContainerFactory = (function () {
    /**
     * Initializes factory dictionary
     */
    function ContainerFactory() {
    }
    /**
     * Builds object depending on className
     * @param className
     * @param params
     */
    ContainerFactory.prototype.build = function (className, params) {
        // Return new class of type className, if not found
        // return object with class of set default class
        return className in Meta_1.Meta.types ? new Meta_1.Meta.types[className](params) : new Meta_1.Meta.types['default'](params);
    };
    return ContainerFactory;
}());
exports.ContainerFactory = ContainerFactory;
