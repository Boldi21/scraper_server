"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var BaseContainer_1 = require("./BaseContainer");
var ImageData_1 = require("../DataModels/ImageData");
var Decorator_1 = require("../Decorator");
/**
 * Container used for Image elements
 */
var ImageContainer = (function (_super) {
    __extends(ImageContainer, _super);
    /**
     * Initializes parent element
     * @param parent
     * @param name
     * @param selector
     * @param hasChildren
     * @param single
     * @param hasData
     */
    function ImageContainer(_a) {
        var parent = _a.parent, name = _a.name, selector = _a.selector, hasChildren = _a.hasChildren, single = _a.single, hasData = _a.hasData;
        // Call parent constructor
        _super.call(this, { parent: parent, name: name, selector: selector, hasChildren: hasChildren, single: single, hasData: hasData });
    }
    /**
     * Builds data object from result
     * @param result
     * @param $
     */
    ImageContainer.prototype.buildDataObject = function (result, $) {
        // Load attributes of result
        var attr = $(result).attr();
        // Initialize new TextData object
        var data = new ImageData_1.ImageData();
        // Set object properties to result's data
        data.src = attr.src.trim();
        data.alt = attr.alt.trim();
        return data;
    };
    ImageContainer = __decorate([
        Decorator_1.Container('Image')
    ], ImageContainer);
    return ImageContainer;
}(BaseContainer_1.BaseContainer));
exports.ImageContainer = ImageContainer;
