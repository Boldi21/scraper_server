import {BaseContainer} from "./BaseContainer";
// Utility imports
import {FileHandler} from "../Utilities/FileHandler";
let stringify = require("json-stringify-pretty-compact")
let CircularJSON = require('circular-json');

/**
 * Root element on every parsing process
 */
export class Root extends BaseContainer{
    /**
     * Initializes Root
     */
    constructor({name, selector}: {name: string, selector: string}) {
        // Default Root setup
        super({
            parent: null,
            name: name,
            selector: selector,
            hasChildren: true,
            single: false,
            hasData: false
        });

        this.once('processingComplete', () => {
            // Create file handler to handle writing result
            let fileHandler = new FileHandler(name + '.json', "./Files/results/", ["write", "create", "rewrite"]);
            let decircledJSON = CircularJSON.stringify(this);
            let parsedJSON = JSON.parse(decircledJSON);

            // Write result
            fileHandler.write(stringify(parsedJSON));

            this.emit('scrapingComplete');

        });

    }

    buildDataObject(){};
}