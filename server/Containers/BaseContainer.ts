import {isArray, isObject, isNullOrUndefined, isString} from "util";
import {Container} from "../Decorator";
import {ContainerFactory} from "../Factories/ContainerFactory";
import {Fetcher} from "../Scraper/Fetcher";
import {EventEmitter} from "events";


let cheerio = require('cheerio');

/**
 * Base Abstract class for elements
 */
@Container('default')
export abstract class BaseContainer extends EventEmitter{

    // Parent element
    private _parent: Object | null;

    // Readable of the element [shown to the user]
    private _name: string;

    // CSS Selector
    private _selector: string;

    // Whether has child elements or not
    private _hasChildren: boolean;

    // Whether it has multiple children or not
    private _single: boolean;

    // Children elements
    protected _children: Array<Object> | null;

    // Whether current element contains data or not
    private _hasData: boolean;

    // Data elements
    protected _data: Array<Object> | null;

    // Whether current element contains paginated elements
    private _pagination: boolean;

    // Current element's pagination selector
    private _paginationSelector: string;

    // Children with the processing complete
    protected _completeChildren: number = 0;

    // Holds the count of children of this element
    protected _numberOfChildren: number;

    /**
     * Initializes element
     * @param parent
     * @param name
     * @param selector
     * @param hasChildren
     * @param single
     * @param hasData
     */
    constructor({parent, name, selector, hasChildren, single, hasData} :
        {parent: Object | null, name: string, selector: string; hasChildren:
            boolean; single: boolean; hasData: boolean}) {

        super();

        // Initialize variables
        this._parent = parent;
        this._name = name;
        this._selector = selector;
        this._hasChildren = hasChildren;
        this._single = single;
        this._hasData = hasData;

        // Initialize children
        this._children = hasChildren === true ? new Array<Object>() : null;
        // Initialize data
        this._data = hasData === true ? new Array<Object>() : null;
    }

    /*
    GETTERS AND SETTES
     */

    /**
     * Returns parent of element
     * @returns {Object}
     */
    get parent(): Object {
        return this._parent;
    }

    /**
     * Returns name of element
     * @returns {string}
     */
    get name(): string {
        return this._name;
    }

    /**
     * Returns selector of element
     * @returns {string}
     */
    get selector(): string {
        return this._selector;
    }

    /**
     * Returns whether element has children or not
     * @returns {boolean}
     */
    get hasChildren(): boolean {
        return this._hasChildren;
    }

    /**
     * Returns whether element has one child element or not
     * @returns {boolean}
     */
    get single(): boolean {
        return this._single;
    }

    /**
     * Returns children of element
     * @returns {Array<Object>}
     */
    get children(): Array<Object> | null {
        return isArray(this._children) ? this._children : null;
    }

    /**
     * Returns whether this element contains data or not
     * @returns {boolean}
     */
    get hasData(): boolean {
        return this._hasData;
    }

    /**
     * Returns data, if doesn't have data returns null
     * @returns {Array<Object>|null}
     */
    get data(): Array<Object> | null {
        return isArray(this._data) ? this._data : null;
    }

    /***
     * Returns whether this element is paginated or not
     * @returns {boolean}
     */
    get pagination(): boolean {
        return this._pagination;
    }

    /**
     * Returns pagination selector
     * @returns {string}
     */
    get paginationSelector(): string {
        return this._paginationSelector;
    }


    /**
     * Sets pagination status for element
     * @param pagination
     */
    set pagination(pagination: boolean) {
        this._pagination = pagination;
    }

    /**
     * Sets pagination selector for element
     * @param paginationSelector
     */
    set paginationSelector(paginationSelector: string) {
        this._paginationSelector = paginationSelector;
    }


    /*
    UTILITY FUNCTIONS
     */

    /**
     * Adds one child to the element's children array
     * @param child
     */
    addChild(child: Object): void {
        this._children.push(child);
    }

    /**
     * Returns last child from array of children
     * @returns {Object}
     */
    lastChild(): any {
        return this._children[this._children.length - 1];
    }

    /**
     * Adds data to element's data array
     * @param data
     */
    addData(data: Object): void {
        this._data.push(data);
    }

    /*
    PROCESSING FUNCTIONS
     */

    /**
     * Processes children and data of node from template
     * @param node
     * @param jQuery
     */
    process(node: Object, jQuery: any) {

        // If it called on null or undefined, return
        if (isNullOrUndefined(node)) {
            return;
        }

        // Process children of node if node has children
        if (this.hasChildren) {
            this.processChildren(node, jQuery);
        }

        // Process data of the node if node has data
        if (this.hasData) {
            this.processData(jQuery);
        }

    }

    /**
     * Loops through node's children and
     * starts processing for each children
     * @param node
     * @param jQuery
     */
    private processChildren(node: Object, jQuery: any) {

        // Instantiate a container factory
        let factory = new ContainerFactory();

        this._numberOfChildren = node.children.length;

        // Loop through node's children
        for (let i = 0; i < node.children.length; ++i) {
            let child = node.children[i];
            // Set up object for new child
            let childInfo = this.buildNodeObject(child);
            // Add child using Container Factory to build
            // object of desired type (given in template)
            this.addChild(factory.build(child.type, childInfo));

            // Listen to child's processing
            this.lastChild().on('processingComplete', () => {
                // Increment the number of children complete
                this._completeChildren += 1;
                // If the number of children complete matches the number
                // of children, fire processingComplete event
                if (this._completeChildren === this._numberOfChildren) {
                    this.emit('processingComplete');
                }
            });

            // Process the added child
            this.lastChild().process(child, jQuery);
        }

        // If there is pagination
        if (this.pagination) {
            // Process next page
            this.processNextPage(jQuery);
        }

    }

    /**
     * Processes matching nodes
     * @param jQuery
     */
    processData(jQuery: any) {
        // Load found nodes into cheerio for children processing
        let $ = jQuery;

        // Get results for query
        let results = jQuery(this.selector);

        // If no results, return
        if (results.length === 0) {
            return;
        }

        // If the parent is single, create one child
        if (this.parent.single && this.parent) {
            // Build data object from result
            let data = this.buildDataObject(results[0], $);
            // Add TextData object to data array
            this._data.push(data);
        } else {
            // Else create multiple children
            for (let i = 0; i < results.length; ++i) {
                // Build data object from result
                let data = this.buildDataObject(results[i], $);
                // Add TextData object to data array
                this._data.push(data);
            }
        }

        // Let the parent know this child is complete
        this.emit('processingComplete');
    }

    /**
     * Builds an object from a template node
     * This object is to be passed to a container constructor
     * @param {Object} node
     */
    private buildNodeObject(node: any): any {

        // If it's not object return
        if (!isObject(node)) {
            return false;
        }

        return {
            parent: this,
            name: node.name,
            selector: this.selector + ' ' + node.selector,
            hasChildren: node.hasChildren,
            single: node.single,
            hasData: node.hasData
        };
    }

    /**
     * Process next page of data
     * @param jQuery
     */
    private processNextPage(jQuery: any) {

        // Double-check if parent has pagination enabled
        if (!this.pagination) {
            return;
        }

        // Load jQuery to $ for easier syntax
        let $ = jQuery;

        // If attributes are undefined, return
        // and set element as complete
        if (!$(this.paginationSelector).attr()) {
            this.emit('processingComplete');
            return;
        }

        // Get next page url
        let nextPageUrl = $(this.paginationSelector).attr().href;

        console.log('Next page URL: ' + nextPageUrl);

        // Check if we got the url for the next page
        if (isString(nextPageUrl)) {
            let fetcher = new Fetcher([nextPageUrl]);
            fetcher.fetchNext(this.processNewPageData, this);
        }

        return;

    }

    /**
     * Processes newly scraped page when results come in
     * @param data
     * @param err
     */
    private processNewPageData(node, err, data) {

        // If an error occurred, throw
        if (err) {
            throw err;
        }

        let jQuery = cheerio.load(data);

        // Process the data if it's set
        if (!isNullOrUndefined(data)) {
            for (let child of node.children) {
                child.processData(jQuery);
            }

            // Process next page if there is any
            node.processNextPage(jQuery);
        }

    }

    /**
     * Builds the data object from result,
     * which goes into the container's data
     * @abstract
     */
    abstract buildDataObject(result, $);
}