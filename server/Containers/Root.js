"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseContainer_1 = require("./BaseContainer");
// Utility imports
var FileHandler_1 = require("../Utilities/FileHandler");
var stringify = require("json-stringify-pretty-compact");
var CircularJSON = require('circular-json');
/**
 * Root element on every parsing process
 */
var Root = (function (_super) {
    __extends(Root, _super);
    /**
     * Initializes Root
     */
    function Root(_a) {
        var _this = this;
        var name = _a.name, selector = _a.selector;
        // Default Root setup
        _super.call(this, {
            parent: null,
            name: name,
            selector: selector,
            hasChildren: true,
            single: false,
            hasData: false
        });
        this.once('processingComplete', function () {
            // Create file handler to handle writing result
            var fileHandler = new FileHandler_1.FileHandler(name + '.json', "./Files/results/", ["write", "create", "rewrite"]);
            var decircledJSON = CircularJSON.stringify(_this);
            var parsedJSON = JSON.parse(decircledJSON);
            // Write result
            fileHandler.write(stringify(parsedJSON));
            _this.emit('scrapingComplete');
        });
    }
    Root.prototype.buildDataObject = function () { };
    ;
    return Root;
}(BaseContainer_1.BaseContainer));
exports.Root = Root;
