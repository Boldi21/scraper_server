import {Container} from "../Decorator";
import {TextData} from "../DataModels/TextData";
import {BaseContainer} from "./BaseContainer";
/**
 * Container used for HTML tags containing text
 * Suggested for: p, span, h1-h6, legend etc.
 */
@Container('Text')
export class TextContainer extends BaseContainer {

    /**
     * Initializes parent element
     * @param parent
     * @param name
     * @param selector
     * @param hasChildren
     * @param single
     * @param hasData
     */
    constructor({parent, name, selector, hasChildren, single, hasData} :
        {parent: Object | null, name: string, selector: string; hasChildren: boolean;
            single: boolean; hasData: boolean}) {

        // Call parent constructor
        super({parent, name, selector, hasChildren, single, hasData});

    }

    /**
     * Builds data object from result
     * @param result
     * @param $
     */
    buildDataObject(result: Object, $: any) {
        // Get properties for result object
        let text = $(result).text().trim();

        // Initialize new TextData object
        let data = new TextData();

        // Set object properties to result's data
        data.textContent = text;

        return data;
    }
}