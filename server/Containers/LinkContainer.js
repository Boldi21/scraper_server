"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var BaseContainer_1 = require("./BaseContainer");
var Decorator_1 = require("../Decorator");
var LinkData_1 = require("../DataModels/LinkData");
/**
 * Container used for Link elements
 */
var LinkContainer = (function (_super) {
    __extends(LinkContainer, _super);
    /**
     * Initializes parent element
     * @param parent
     * @param name
     * @param selector
     * @param hasChildren
     * @param single
     * @param hasData
     */
    function LinkContainer(_a) {
        var parent = _a.parent, name = _a.name, selector = _a.selector, hasChildren = _a.hasChildren, single = _a.single, hasData = _a.hasData;
        // Call parent constructor
        _super.call(this, { parent: parent, name: name, selector: selector, hasChildren: hasChildren, single: single, hasData: hasData });
    }
    /**
     * Builds data object from result
     * @param result
     * @param $
     */
    LinkContainer.prototype.buildDataObject = function (result, $) {
        // Load attributes of result
        var attr = $(result).attr();
        // Initialize new TextData object
        var data = new LinkData_1.LinkData();
        // Set object properties to result's data
        data.href = attr.href.trim();
        data.textContent = attr.title.trim();
        return data;
    };
    LinkContainer = __decorate([
        Decorator_1.Container('Link')
    ], LinkContainer);
    return LinkContainer;
}(BaseContainer_1.BaseContainer));
exports.LinkContainer = LinkContainer;
