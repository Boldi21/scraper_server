import {BaseContainer} from "./BaseContainer";
import {Container} from "../Decorator";
import {LinkData} from "../DataModels/LinkData";

/**
 * Container used for Link elements
 */
@Container('Link')
export class LinkContainer extends BaseContainer {

    /**
     * Initializes parent element
     * @param parent
     * @param name
     * @param selector
     * @param hasChildren
     * @param single
     * @param hasData
     */
    constructor({parent, name, selector, hasChildren, single, hasData} :
        {parent: Object | null, name: string, selector: string; hasChildren: boolean;
            single: boolean; hasData: boolean}) {
        // Call parent constructor
        super({parent, name, selector, hasChildren, single, hasData});

    }

    /**
     * Builds data object from result
     * @param result
     * @param $
     */
    buildDataObject(result: Object, $: any) {
        // Load attributes of result
        let attr = $(result).attr();

        // Initialize new TextData object
        let data = new LinkData();

        // Set object properties to result's data
        data.href = attr.href.trim();
        data.textContent = attr.title.trim();

        return data;
    }


}