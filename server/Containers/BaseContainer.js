"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var util_1 = require("util");
var Decorator_1 = require("../Decorator");
var ContainerFactory_1 = require("../Factories/ContainerFactory");
var Fetcher_1 = require("../Scraper/Fetcher");
var events_1 = require("events");
var cheerio = require('cheerio');
/**
 * Base Abstract class for elements
 */
var BaseContainer = (function (_super) {
    __extends(BaseContainer, _super);
    /**
     * Initializes element
     * @param parent
     * @param name
     * @param selector
     * @param hasChildren
     * @param single
     * @param hasData
     */
    function BaseContainer(_a) {
        var parent = _a.parent, name = _a.name, selector = _a.selector, hasChildren = _a.hasChildren, single = _a.single, hasData = _a.hasData;
        _super.call(this);
        // Children with the processing complete
        this._completeChildren = 0;
        // Initialize variables
        this._parent = parent;
        this._name = name;
        this._selector = selector;
        this._hasChildren = hasChildren;
        this._single = single;
        this._hasData = hasData;
        // Initialize children
        this._children = hasChildren === true ? new Array() : null;
        // Initialize data
        this._data = hasData === true ? new Array() : null;
    }
    Object.defineProperty(BaseContainer.prototype, "parent", {
        /*
        GETTERS AND SETTES
         */
        /**
         * Returns parent of element
         * @returns {Object}
         */
        get: function () {
            return this._parent;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseContainer.prototype, "name", {
        /**
         * Returns name of element
         * @returns {string}
         */
        get: function () {
            return this._name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseContainer.prototype, "selector", {
        /**
         * Returns selector of element
         * @returns {string}
         */
        get: function () {
            return this._selector;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseContainer.prototype, "hasChildren", {
        /**
         * Returns whether element has children or not
         * @returns {boolean}
         */
        get: function () {
            return this._hasChildren;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseContainer.prototype, "single", {
        /**
         * Returns whether element has one child element or not
         * @returns {boolean}
         */
        get: function () {
            return this._single;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseContainer.prototype, "children", {
        /**
         * Returns children of element
         * @returns {Array<Object>}
         */
        get: function () {
            return util_1.isArray(this._children) ? this._children : null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseContainer.prototype, "hasData", {
        /**
         * Returns whether this element contains data or not
         * @returns {boolean}
         */
        get: function () {
            return this._hasData;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseContainer.prototype, "data", {
        /**
         * Returns data, if doesn't have data returns null
         * @returns {Array<Object>|null}
         */
        get: function () {
            return util_1.isArray(this._data) ? this._data : null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseContainer.prototype, "pagination", {
        /***
         * Returns whether this element is paginated or not
         * @returns {boolean}
         */
        get: function () {
            return this._pagination;
        },
        /**
         * Sets pagination status for element
         * @param pagination
         */
        set: function (pagination) {
            this._pagination = pagination;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseContainer.prototype, "paginationSelector", {
        /**
         * Returns pagination selector
         * @returns {string}
         */
        get: function () {
            return this._paginationSelector;
        },
        /**
         * Sets pagination selector for element
         * @param paginationSelector
         */
        set: function (paginationSelector) {
            this._paginationSelector = paginationSelector;
        },
        enumerable: true,
        configurable: true
    });
    /*
    UTILITY FUNCTIONS
     */
    /**
     * Adds one child to the element's children array
     * @param child
     */
    BaseContainer.prototype.addChild = function (child) {
        this._children.push(child);
    };
    /**
     * Returns last child from array of children
     * @returns {Object}
     */
    BaseContainer.prototype.lastChild = function () {
        return this._children[this._children.length - 1];
    };
    /**
     * Adds data to element's data array
     * @param data
     */
    BaseContainer.prototype.addData = function (data) {
        this._data.push(data);
    };
    /*
    PROCESSING FUNCTIONS
     */
    /**
     * Processes children and data of node from template
     * @param node
     * @param jQuery
     */
    BaseContainer.prototype.process = function (node, jQuery) {
        // If it called on null or undefined, return
        if (util_1.isNullOrUndefined(node)) {
            return;
        }
        // Process children of node if node has children
        if (this.hasChildren) {
            this.processChildren(node, jQuery);
        }
        // Process data of the node if node has data
        if (this.hasData) {
            this.processData(jQuery);
        }
    };
    /**
     * Loops through node's children and
     * starts processing for each children
     * @param node
     * @param jQuery
     */
    BaseContainer.prototype.processChildren = function (node, jQuery) {
        var _this = this;
        // Instantiate a container factory
        var factory = new ContainerFactory_1.ContainerFactory();
        this._numberOfChildren = node.children.length;
        // Loop through node's children
        for (var i = 0; i < node.children.length; ++i) {
            var child = node.children[i];
            // Set up object for new child
            var childInfo = this.buildNodeObject(child);
            // Add child using Container Factory to build
            // object of desired type (given in template)
            this.addChild(factory.build(child.type, childInfo));
            // Listen to child's processing
            this.lastChild().on('processingComplete', function () {
                // Increment the number of children complete
                _this._completeChildren += 1;
                // If the number of children complete matches the number
                // of children, fire processingComplete event
                if (_this._completeChildren === _this._numberOfChildren) {
                    _this.emit('processingComplete');
                }
            });
            // Process the added child
            this.lastChild().process(child, jQuery);
        }
        // If there is pagination
        if (this.pagination) {
            // Process next page
            this.processNextPage(jQuery);
        }
    };
    /**
     * Processes matching nodes
     * @param jQuery
     */
    BaseContainer.prototype.processData = function (jQuery) {
        // Load found nodes into cheerio for children processing
        var $ = jQuery;
        // Get results for query
        var results = jQuery(this.selector);
        // If no results, return
        if (results.length === 0) {
            return;
        }
        // If the parent is single, create one child
        if (this.parent.single && this.parent) {
            // Build data object from result
            var data = this.buildDataObject(results[0], $);
            // Add TextData object to data array
            this._data.push(data);
        }
        else {
            // Else create multiple children
            for (var i = 0; i < results.length; ++i) {
                // Build data object from result
                var data = this.buildDataObject(results[i], $);
                // Add TextData object to data array
                this._data.push(data);
            }
        }
        // Let the parent know this child is complete
        this.emit('processingComplete');
    };
    /**
     * Builds an object from a template node
     * This object is to be passed to a container constructor
     * @param {Object} node
     */
    BaseContainer.prototype.buildNodeObject = function (node) {
        // If it's not object return
        if (!util_1.isObject(node)) {
            return false;
        }
        return {
            parent: this,
            name: node.name,
            selector: this.selector + ' ' + node.selector,
            hasChildren: node.hasChildren,
            single: node.single,
            hasData: node.hasData
        };
    };
    /**
     * Process next page of data
     * @param jQuery
     */
    BaseContainer.prototype.processNextPage = function (jQuery) {
        // Double-check if parent has pagination enabled
        if (!this.pagination) {
            return;
        }
        // Load jQuery to $ for easier syntax
        var $ = jQuery;
        // If attributes are undefined, return
        // and set element as complete
        if (!$(this.paginationSelector).attr()) {
            this.emit('processingComplete');
            return;
        }
        // Get next page url
        var nextPageUrl = $(this.paginationSelector).attr().href;
        console.log('Next page URL: ' + nextPageUrl);
        // Check if we got the url for the next page
        if (util_1.isString(nextPageUrl)) {
            var fetcher = new Fetcher_1.Fetcher([nextPageUrl]);
            fetcher.fetchNext(this.processNewPageData, this);
        }
        return;
    };
    /**
     * Processes newly scraped page when results come in
     * @param data
     * @param err
     */
    BaseContainer.prototype.processNewPageData = function (node, err, data) {
        // If an error occurred, throw
        if (err) {
            throw err;
        }
        var jQuery = cheerio.load(data);
        // Process the data if it's set
        if (!util_1.isNullOrUndefined(data)) {
            for (var _i = 0, _a = node.children; _i < _a.length; _i++) {
                var child = _a[_i];
                child.processData(jQuery);
            }
            // Process next page if there is any
            node.processNextPage(jQuery);
        }
    };
    BaseContainer = __decorate([
        Decorator_1.Container('default')
    ], BaseContainer);
    return BaseContainer;
}(events_1.EventEmitter));
exports.BaseContainer = BaseContainer;
