import {BaseContainer} from "./BaseContainer";
import {ImageData} from "../DataModels/ImageData";
import {Container} from "../Decorator";

/**
 * Container used for Image elements
 */
@Container('Image')
export class ImageContainer extends BaseContainer {

    /**
     * Initializes parent element
     * @param parent
     * @param name
     * @param selector
     * @param hasChildren
     * @param single
     * @param hasData
     */
    constructor({parent, name, selector, hasChildren, single, hasData} :
        {parent: Object | null, name: string, selector: string; hasChildren: boolean;
            single: boolean; hasData: boolean}) {
        // Call parent constructor
        super({parent, name, selector, hasChildren, single, hasData});

    }

    /**
     * Builds data object from result
     * @param result
     * @param $
     */
    buildDataObject(result: Object, $: any) {
        // Load attributes of result
        let attr = $(result).attr();

        // Initialize new TextData object
        let data = new ImageData();

        // Set object properties to result's data
        data.src = attr.src.trim();
        data.alt = attr.alt.trim();

        return data;
    }


}