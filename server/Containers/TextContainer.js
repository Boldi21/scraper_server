"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var Decorator_1 = require("../Decorator");
var TextData_1 = require("../DataModels/TextData");
var BaseContainer_1 = require("./BaseContainer");
/**
 * Container used for HTML tags containing text
 * Suggested for: p, span, h1-h6, legend etc.
 */
var TextContainer = (function (_super) {
    __extends(TextContainer, _super);
    /**
     * Initializes parent element
     * @param parent
     * @param name
     * @param selector
     * @param hasChildren
     * @param single
     * @param hasData
     */
    function TextContainer(_a) {
        var parent = _a.parent, name = _a.name, selector = _a.selector, hasChildren = _a.hasChildren, single = _a.single, hasData = _a.hasData;
        // Call parent constructor
        _super.call(this, { parent: parent, name: name, selector: selector, hasChildren: hasChildren, single: single, hasData: hasData });
    }
    /**
     * Builds data object from result
     * @param result
     * @param $
     */
    TextContainer.prototype.buildDataObject = function (result, $) {
        // Get properties for result object
        var text = $(result).text().trim();
        // Initialize new TextData object
        var data = new TextData_1.TextData();
        // Set object properties to result's data
        data.textContent = text;
        return data;
    };
    TextContainer = __decorate([
        Decorator_1.Container('Text')
    ], TextContainer);
    return TextContainer;
}(BaseContainer_1.BaseContainer));
exports.TextContainer = TextContainer;
