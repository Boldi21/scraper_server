"use strict";
/**
 * Error for trying to perform action on non-existent file
 */
var FileNotFoundError = (function () {
    function FileNotFoundError(message, extra) {
        Error.captureStackTrace(this, FileNotFoundError.constructor);
        this.name = this.constructor.name;
        this.message = message;
        this.extra = extra;
    }
    return FileNotFoundError;
}());
exports.FileNotFoundError = FileNotFoundError;
