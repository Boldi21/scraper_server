/**
 * Error on file read
 */
export class FileReadError implements Error {

    name: string;
    message: string;
    extra: any;

    constructor(message, extra) {
        Error.captureStackTrace(this, FileReadError.constructor);
        this.name = this.constructor.name;
        this.message = message;
        this.extra = extra;
    }

}