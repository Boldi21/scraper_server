/**
 * Error for trying to perform read file without permission
 */
export class FileReadPermissionError implements Error {

    name: string;
    message: string;
    extra: any;

    constructor(message, extra) {
        Error.captureStackTrace(this, FileReadPermissionError.constructor);
        this.name = this.constructor.name;
        this.message = message;
        this.extra = extra;
    }

}