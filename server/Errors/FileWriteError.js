"use strict";
/**
 * Error on file write
 */
var FileWriteError = (function () {
    function FileWriteError(message, extra) {
        Error.captureStackTrace(this, FileWriteError.constructor);
        this.name = this.constructor.name;
        this.message = message;
        this.extra = extra;
    }
    return FileWriteError;
}());
exports.FileWriteError = FileWriteError;
