/**
 * Error on file write
 */
export class FileWriteError implements Error {

    name: string;
    message: string;
    extra: any;

    constructor(message, extra) {
        Error.captureStackTrace(this, FileWriteError.constructor);
        this.name = this.constructor.name;
        this.message = message;
        this.extra = extra;
    }

}