"use strict";
/**
 * Error for trying to perform create and write to a file
 */
var FileCreateWritePermissionError = (function () {
    function FileCreateWritePermissionError(message, extra) {
        Error.captureStackTrace(this, FileCreateWritePermissionError.constructor);
        this.name = this.constructor.name;
        this.message = message;
        this.extra = extra;
    }
    return FileCreateWritePermissionError;
}());
exports.FileCreateWritePermissionError = FileCreateWritePermissionError;
