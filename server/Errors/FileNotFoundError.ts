/**
 * Error for trying to perform action on non-existent file
 */
export class FileNotFoundError implements Error {

    name: string;
    message: string;
    extra: any;

    constructor(message, extra) {
        Error.captureStackTrace(this, FileNotFoundError.constructor);
        this.name = this.constructor.name;
        this.message = message;
        this.extra = extra;
    }

}