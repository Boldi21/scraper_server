"use strict";
/**
 * Error for trying to perform read file without permission
 */
var FileReadPermissionError = (function () {
    function FileReadPermissionError(message, extra) {
        Error.captureStackTrace(this, FileReadPermissionError.constructor);
        this.name = this.constructor.name;
        this.message = message;
        this.extra = extra;
    }
    return FileReadPermissionError;
}());
exports.FileReadPermissionError = FileReadPermissionError;
