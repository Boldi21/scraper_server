"use strict";
/**
 * Error on file read
 */
var FileReadError = (function () {
    function FileReadError(message, extra) {
        Error.captureStackTrace(this, FileReadError.constructor);
        this.name = this.constructor.name;
        this.message = message;
        this.extra = extra;
    }
    return FileReadError;
}());
exports.FileReadError = FileReadError;
