/**
 * Error for trying to perform create and write to a file
 */
export class FileCreateWritePermissionError implements Error {

    name: string;
    message: string;
    extra: any;

    constructor(message, extra) {
        Error.captureStackTrace(this, FileCreateWritePermissionError.constructor);
        this.name = this.constructor.name;
        this.message = message;
        this.extra = extra;
    }

}