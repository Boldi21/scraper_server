/**
 * Error for trying to rewrite file without permission
 */
export class FileRewritePermissionError implements Error {

    name: string;
    message: string;
    extra: any;

    constructor(message, extra) {
        Error.captureStackTrace(this, FileRewritePermissionError.constructor);
        this.name = this.constructor.name;
        this.message = message;
        this.extra = extra;
    }

}