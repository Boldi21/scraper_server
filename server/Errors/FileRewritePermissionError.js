"use strict";
/**
 * Error for trying to rewrite file without permission
 */
var FileRewritePermissionError = (function () {
    function FileRewritePermissionError(message, extra) {
        Error.captureStackTrace(this, FileRewritePermissionError.constructor);
        this.name = this.constructor.name;
        this.message = message;
        this.extra = extra;
    }
    return FileRewritePermissionError;
}());
exports.FileRewritePermissionError = FileRewritePermissionError;
