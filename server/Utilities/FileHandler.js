"use strict";
// Imports
var FileReadPermissionError_1 = require("../Errors/FileReadPermissionError");
var FileNotFoundError_1 = require("../Errors/FileNotFoundError");
var FileReadError_1 = require("../Errors/FileReadError");
var FileWriteError_1 = require("../Errors/FileWriteError");
var FileRewritePermissionError_1 = require("../Errors/FileRewritePermissionError");
var FileCreateWritePermissionError_1 = require("../Errors/FileCreateWritePermissionError");
var fs = require('fs');
/**
 * Utility class for handling files
 */
var FileHandler = (function () {
    /**
     * Creates an instance of FileHandler.
     * @param {string} fileName
     * @param {string} filePath
     * @param {string} permissions
     *
     * @memberOf FileHandler
     */
    function FileHandler(fileName, filePath, permissions) {
        this._canRead = false;
        this._canWrite = false;
        this._canRewrite = false;
        this._canCreate = false;
        this._fileName = fileName;
        this._filePath = filePath;
        this._fullFilePath = this._filePath + this._fileName;
        this._permissions = permissions;
        // Set permissions for this FileHandler
        this.setPermissions();
    }
    /**
     * Set permsissions for the filehandler
     *
     * @private
     *
     * @memberOf FileHandler
     */
    FileHandler.prototype.setPermissions = function () {
        // If read permission is given
        if (this._permissions.some(function (x) { return x === "read"; })) {
            this._canRead = true;
        }
        // If write permission is given
        if (this._permissions.some(function (x) { return x === "write"; })) {
            this._canWrite = true;
        }
        // If rewrite permission is given
        if (this._permissions.some(function (x) { return x === "rewrite"; })) {
            this._canRewrite = true;
        }
        // If create permission is given
        if (this._permissions.some(function (x) { return x === "create"; })) {
            this._canCreate = true;
        }
    };
    /**
     * Writes/Rewrites content to file if permissions are given
     *
     * @param {string} content
     *
     * @memberOf FileHandler
     */
    FileHandler.prototype.write = function (content) {
        // Check if file already exists
        if (fs.existsSync(this._fullFilePath)) {
            // Check if it has rewrite permissions
            if (this._canRewrite) {
                return this.writeFile(content);
            }
            else {
                throw new FileRewritePermissionError_1.FileRewritePermissionError('Rewrite permissions not given for file: ' + this._fullFilePath, '');
            }
        }
        else {
            // Check if write permission is given
            if (this._canWrite && this._canCreate) {
                return this.writeFile(content);
            }
            else {
                throw new FileCreateWritePermissionError_1.FileCreateWritePermissionError('Write/Create permissions are not given for file: ' + this._fullFilePath, '');
            }
        }
    };
    /**
     * Writes content to file
     *
     * @param {string} content
     */
    FileHandler.prototype.writeFile = function (content) {
        var _this = this;
        fs.writeFileSync(this._fullFilePath, content, 'utf8', function (err) {
            if (err)
                throw new FileWriteError_1.FileWriteError(_this._fullFilePath, err);
            return true;
        });
    };
    /**
     * Reads content of file in given encoding if permissions are given
     *
     * @param {string} encoding
     */
    FileHandler.prototype.read = function (encoding) {
        // If read permission is given
        if (this._canRead) {
            // If file exists
            if (fs.existsSync(this._filePath + this._fileName)) {
                this._fileContent = this.readFile();
                // Check encoding setting and return in requested encoding
                switch (encoding) {
                    case 'JSON':
                        // If it's not JSON already, parse to JSON
                        if (typeof this._fileContent != 'object') {
                            this._fileContent = JSON.parse(this._fileContent);
                        }
                        return this._fileContent;
                    default:
                        // In case of no encoding return as string
                        return this._fileContent;
                }
            }
            else {
                throw new FileNotFoundError_1.FileNotFoundError("File not found: " + this._fullFilePath, '');
            }
        }
        else {
            throw new FileReadPermissionError_1.FileReadPermissionError("Read permissions not given for file: " + this._fullFilePath, '');
        }
    };
    /**
     * Reads file in given encoding
     */
    FileHandler.prototype.readFile = function () {
        var _this = this;
        return fs.readFileSync(this._fullFilePath, 'utf8', function (err, data) {
            // If error has occurred
            if (err)
                throw new FileReadError_1.FileReadError("Error in reading file " + _this._fullFilePath, err);
            return data;
        });
    };
    return FileHandler;
}());
exports.FileHandler = FileHandler;
