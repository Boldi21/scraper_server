// Imports
import {FileReadPermissionError} from "../Errors/FileReadPermissionError";
import {FileNotFoundError} from "../Errors/FileNotFoundError";
import {FileReadError} from "../Errors/FileReadError";
import {FileWriteError} from "../Errors/FileWriteError";
import {FileRewritePermissionError} from "../Errors/FileRewritePermissionError";
import {FileCreateWritePermissionError} from "../Errors/FileCreateWritePermissionError";
var fs = require('fs');

/**
 * Utility class for handling files
 */
export class FileHandler {

    private _fileName: string;
    private _filePath: string;
    private _fullFilePath: string;
    private _permissions: Array<string>;

    private _canRead: boolean = false;
    private _canWrite: boolean = false;
    private _canRewrite: boolean = false;
    private _canCreate: boolean = false;

    private _fileContent: any;
    
    /**
     * Creates an instance of FileHandler.
     * @param {string} fileName 
     * @param {string} filePath 
     * @param {string} permissions 
     * 
     * @memberOf FileHandler
     */
	constructor(fileName: string, filePath: string, permissions: Array<string>) {
		this._fileName = fileName;
		this._filePath = filePath;
		this._fullFilePath = this._filePath + this._fileName;
		this._permissions = permissions;
        // Set permissions for this FileHandler
        this.setPermissions();
	}

    /**
     * Set permsissions for the filehandler
     * 
     * @private
     * 
     * @memberOf FileHandler
     */
    private setPermissions() {

        // If read permission is given
        if (this._permissions.some(x => x === "read")) {
            this._canRead = true;
        }

        // If write permission is given
        if (this._permissions.some(x => x === "write")) {
            this._canWrite = true;
        }

        // If rewrite permission is given
        if (this._permissions.some(x => x === "rewrite")) {
            this._canRewrite = true;
        }

        // If create permission is given
        if (this._permissions.some(x => x === "create")) {
            this._canCreate = true;
        }

    }

    /**
     * Writes/Rewrites content to file if permissions are given
     * 
     * @param {string} content 
     * 
     * @memberOf FileHandler
     */
    public write(content: string) {

        // Check if file already exists
        if (fs.existsSync(this._fullFilePath)) {
            // Check if it has rewrite permissions
            if (this._canRewrite) {
                return this.writeFile(content);
            } else {
                throw new FileRewritePermissionError('Rewrite permissions not given for file: ' + this._fullFilePath, '');
            }
        } else {
            // Check if write permission is given
            if (this._canWrite && this._canCreate) {
                return this.writeFile(content);
            } else {
                throw new FileCreateWritePermissionError('Write/Create permissions are not given for file: ' + this._fullFilePath, '');
            }
        }

    }

    /**
     * Writes content to file
     *
     * @param {string} content
     */
    private writeFile(content: string) {
        fs.writeFileSync(this._fullFilePath, content, 'utf8', (err) => {
            if (err) throw new FileWriteError(this._fullFilePath, err);
            return true;
        });
    }

    /**
     * Reads content of file in given encoding if permissions are given
     *
     * @param {string} encoding
     */
    public read(encoding?: string) {
        // If read permission is given
        if (this._canRead) {

            // If file exists
            if (fs.existsSync(this._filePath + this._fileName)) {

                this._fileContent = this.readFile();

                // Check encoding setting and return in requested encoding
                switch (encoding) {
                    case 'JSON':
                        // If it's not JSON already, parse to JSON
                        if (typeof this._fileContent != 'object') {
                            this._fileContent = JSON.parse(this._fileContent);
                        }
                        return this._fileContent;

                    default:
                        // In case of no encoding return as string
                        return this._fileContent;

                }
            } else {
                throw new FileNotFoundError("File not found: " + this._fullFilePath, '');
            }

        } else {
            throw new FileReadPermissionError("Read permissions not given for file: " + this._fullFilePath, '');
        }

    }

    /**
     * Reads file in given encoding
     */
    private readFile() {

        return fs.readFileSync(this._fullFilePath, 'utf8' ,(err, data) => {
            // If error has occurred
            if (err) throw new FileReadError("Error in reading file " +  this._fullFilePath, err);
            return data;
        });

    }

}
