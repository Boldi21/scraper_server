"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
// Import for decorator to work
var index_1 = require("../index");
var events_1 = require("events");
// Import for processing
var cheerio = require('cheerio');
/**
 * Parses HTML using given template into valuable information
 *
 * @export
 * @class Parser
 */
var Parser = (function (_super) {
    __extends(Parser, _super);
    /**
     * Creates an instance of Parser.
     * @param {string} body
     * @param {Object} template
     *
     * @memberOf Parser
     */
    function Parser(body, template) {
        _super.call(this);
        this._body = body;
        this._template = template;
        this._jQuery = cheerio.load(body);
    }
    /**
     * Parses body based on given template
     *
     * @memberOf Parser
     */
    Parser.prototype.parse = function () {
        var _this = this;
        // Set the first level of the template to be root
        this._result = new index_1.Root({
            name: this._template.name,
            selector: this._template.selector
        }).once('scrapingComplete', function () {
            _this.emit('complete');
        });
        this._result.pagination = this._template.pagination;
        this._result.paginationSelector = this._template.paginationSelector;
        // Process the Root and it's children
        this._result.process(this._template, this._jQuery);
    };
    // Returns result
    Parser.prototype.result = function () {
        return this._result;
    };
    return Parser;
}(events_1.EventEmitter));
exports.Parser = Parser;
