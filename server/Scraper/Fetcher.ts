// Imports
const request = require('request');


/**
 * Fetches HTML from given URLs
 */
export class Fetcher {

    // Contains list of URLS to be fetched, or
    // already fetched
    private _urls: Array<string>;

    // Holds count of fetches
    private _fetchCount: number;

    /**
     * Constructs Fetcher with list of URLs or empty
     * list
     * @param urls 
     */
    constructor(urls?: Array<string>) {
        if (urls) {
            this._urls = urls;
        } else {
            this._urls = [];
        }
        this._fetchCount = 0;
    }

    /**
     * Adds URL to the URL list to fetch
     * @param url
     */
    public addUrl(url: string) {
        this._urls.push(url);
    }

    /**
     * Fetches next URL in list and calls callback on task done
     *
     * @param callback
     * @param caller
     */
    public fetchNext(callback, caller?: Object) {
        // If there are no more URLs to fetch, throw error
        if (this._fetchCount + 1 > this._urls.length) {
            throw new Error("NO_MORE_URLS");
        }

        // Fetch next URL
        this.fetch(callback, caller);

        this._fetchCount += 1;

    }

    /**
     * Creates promise to fetch next URL in the list
     */
    private fetch(callback, caller?: Object) {

        request(this._urls[this._fetchCount], (error, response, body) => {
            // TODO: Implement Throwables for different fetch errors
            if (error) {
                // Call callback with error
                if (caller) {
                    callback(caller, error, null);
                } else {
                    callback(caller, error, null);
                }
            } else if (response.statusCode == 200) {
                if (caller) {
                    callback(caller, null, body);
                } else {
                    callback(null, body);
                }
            }
        });

    }

    /**
     * Returns whether there are more urls
     * to fetch or not
     * 
     * @returns 
     * 
     * @memberOf Fetcher
     */
    public fetcherEmpty() {
        return this._fetchCount >= this._urls.length;
    }

}

// Snippet for require to work in Typescript
declare function require(name:string);