"use strict";
// Imports
var request = require('request');
/**
 * Fetches HTML from given URLs
 */
var Fetcher = (function () {
    /**
     * Constructs Fetcher with list of URLs or empty
     * list
     * @param urls
     */
    function Fetcher(urls) {
        if (urls) {
            this._urls = urls;
        }
        else {
            this._urls = [];
        }
        this._fetchCount = 0;
    }
    /**
     * Adds URL to the URL list to fetch
     * @param url
     */
    Fetcher.prototype.addUrl = function (url) {
        this._urls.push(url);
    };
    /**
     * Fetches next URL in list and calls callback on task done
     *
     * @param callback
     * @param caller
     */
    Fetcher.prototype.fetchNext = function (callback, caller) {
        // If there are no more URLs to fetch, throw error
        if (this._fetchCount + 1 > this._urls.length) {
            throw new Error("NO_MORE_URLS");
        }
        // Fetch next URL
        this.fetch(callback, caller);
        this._fetchCount += 1;
    };
    /**
     * Creates promise to fetch next URL in the list
     */
    Fetcher.prototype.fetch = function (callback, caller) {
        request(this._urls[this._fetchCount], function (error, response, body) {
            // TODO: Implement Throwables for different fetch errors
            if (error) {
                // Call callback with error
                if (caller) {
                    callback(caller, error, null);
                }
                else {
                    callback(caller, error, null);
                }
            }
            else if (response.statusCode == 200) {
                if (caller) {
                    callback(caller, null, body);
                }
                else {
                    callback(null, body);
                }
            }
        });
    };
    /**
     * Returns whether there are more urls
     * to fetch or not
     *
     * @returns
     *
     * @memberOf Fetcher
     */
    Fetcher.prototype.fetcherEmpty = function () {
        return this._fetchCount >= this._urls.length;
    };
    return Fetcher;
}());
exports.Fetcher = Fetcher;
