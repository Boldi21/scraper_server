// Import for decorator to work
import {Root} from "../index";
import {EventEmitter} from "events";

// Import for processing
let cheerio = require('cheerio');

/**
 * Parses HTML using given template into valuable information
 * 
 * @export
 * @class Parser
 */
export class Parser extends EventEmitter{

    private _body: string;
    private _template: Object;
    private _result: Root;

    // Cheerio for HTML parsing
    private _jQuery: any;

    /**
     * Creates an instance of Parser.
     * @param {string} body 
     * @param {Object} template 
     * 
     * @memberOf Parser
     */
	constructor(body: string, template: Object) {
	    super();

		this._body = body;
		this._template = template;
		this._jQuery = cheerio.load(body);
	}
    
    /**
     * Parses body based on given template
     * 
     * @memberOf Parser
     */
    public parse() {

        // Set the first level of the template to be root
        this._result = new Root({
            name: this._template.name,
            selector: this._template.selector
        }).once('scrapingComplete', () => {
            this.emit('complete');
        });

        this._result.pagination = this._template.pagination;
        this._result.paginationSelector = this._template.paginationSelector;

        // Process the Root and it's children
        this._result.process(this._template, this._jQuery);

    }

    // Returns result
    public result() {
        return this._result;
    }

}