import {Meta} from "./Meta";
/**
 * Decorator for Containers
 * @param className
 * @returns {(target:any)=>undefined}
 * @constructor
 */
export function Container(className: string) {
    return (target: any) => {
        console.log('Adding type ' + className);
        Meta.types[!!className ? className : target.name] = target;
    }
}