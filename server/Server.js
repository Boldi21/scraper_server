"use strict";
// Imports
var BaseRouter_1 = require("./Routers/BaseRouter");
var bodyParser = require('body-parser');
var express = require('express');
/**
 * Server base for NodeJS app
 */
var Server = (function () {
    /**
     * Initializes server on given port
     * @param port
     */
    function Server() {
        var express = require('express');
        this._app = express();
        this._port = process.env.PORT || 5000;
    }
    /**
     * Initializes the routers
     */
    Server.prototype.initializeRouters = function () {
        // // Allow cross-origin for testing
        // this.app.use((req, res, next) => {
        //     res.header('Access-Control-Allow-Origin', '*');
        //     res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        //     res.header('Access-Control-Allow-Headers', 'Content-Type');
        //
        //     next();
        // });
        this.app.use(express.static('public'));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({
            extended: true
        }));
        // ![KEEP AS LAST AS IT CONTAINS CONFIGS]
        // Initialize routes and rules for main app
        var baseRouter = new BaseRouter_1.BaseRouter(this);
    };
    /**
     * Starts the server
     */
    Server.prototype.start = function () {
        var _this = this;
        this.app.listen(this._port, function () {
            console.log('Server running on port: ' + _this._port);
        });
    };
    /**
     * Stops the server
     */
    Server.prototype.stop = function () {
        process.exit();
    };
    Object.defineProperty(Server.prototype, "app", {
        /**
         * Returns express object representing the running instnace
         * of the server
         */
        get: function () {
            return this._app;
        },
        enumerable: true,
        configurable: true
    });
    return Server;
}());
exports.Server = Server;
