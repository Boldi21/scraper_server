// Imports
import {BaseRouter} from "./Routers/BaseRouter";

let bodyParser = require('body-parser');
let express = require('express');

/**
 * Server base for NodeJS app
 */
export class Server {

    // Object of type express
    private _app: any;

    // Port on which the server runs
    private _port: number;

    /**
     * Initializes server on given port
     * @param port 
     */
    constructor() {
        var express = require('express');
        this._app = express();
        this._port = process.env.PORT || 5000;
    }

    /**
     * Initializes the routers
     */
    public initializeRouters():void {

        // // Allow cross-origin for testing
        // this.app.use((req, res, next) => {
        //     res.header('Access-Control-Allow-Origin', '*');
        //     res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        //     res.header('Access-Control-Allow-Headers', 'Content-Type');
        //
        //     next();
        // });

        this.app.use(express.static('public'));

        this.app.use(bodyParser.json());

        this.app.use(bodyParser.urlencoded({
            extended: true
        }));

        // ![KEEP AS LAST AS IT CONTAINS CONFIGS]
        // Initialize routes and rules for main app
        let baseRouter = new BaseRouter(this);
        
    }

    /**
     * Starts the server
     */
    public start():void {

        this.app.listen(this._port,() => {
            console.log('Server running on port: ' + this._port);
        });
        
    }

    /**
     * Stops the server
     */
    public stop(): void {
        process.exit();
    }

    /**
     * Returns express object representing the running instnace
     * of the server
     */
     get app(): any {
         return this._app;
     }

}

// Snippet for require to work in Typescript
declare function require(name:string);
