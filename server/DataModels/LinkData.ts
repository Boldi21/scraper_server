/**
 * Model containing data of href nodes
 */
export class LinkData {

    // Url from within the node
    href: string;

    // Title of url
    textContent: string;

}