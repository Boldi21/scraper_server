/**
 * Model containing data of text nodes
 */
export class TextData {

    // Text from within the node
    textContent: string;

}