/**
 * Model containing data of image nodes
 */
export class ImageData {

    // Source of the image
    src: string;

    // Alternative text for image
    alt: string;

}