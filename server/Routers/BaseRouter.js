"use strict";
var Fetcher_1 = require("../Scraper/Fetcher");
var Parser_1 = require("../Scraper/Parser");
// Utilities
var request = require('request');
/**
 * Router that holds the base routes of the application
 * and sets app-wide routing rules
 */
var BaseRouter = (function () {
    function BaseRouter(server) {
        // Get express object
        var app = server.app;
        console.log('Configuring Base Router...');
        app.use(function (req, res, next) {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
            res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, Accept, Origin');
            // intercept OPTIONS method
            if ('OPTIONS' === req.method) {
                res.send(200);
            }
            else {
                next();
            }
        });
        // Set home page to redirect a welcome message
        app.get('/', function (req, res) {
            res.send('Hello vilag!');
        });
        // Page not found error [404]
        app.get('*', function (req, res) {
            res.status(404).send('404 - Not found');
        });
        // Request for a given link's HTML
        app.post('/get', function (req, res) {
            // Url of the page and regular expression to detect if it's a valid URL
            var url = req.body.url, urlRegex = new RegExp(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/), baseUrl;
            // Get base URL for resource linking purposes
            baseUrl = url.indexOf("://") > -1 ? url.split('://')[0] + '://' : 'https://';
            baseUrl = url.indexOf("://") > -1 ? baseUrl + url.split('/')[2] : baseUrl + url.split('/')[0];
            // Regex for all protocols
            var protocolRegex = new RegExp(/http:/g);
            console.time('Response sent');
            // Remove trailing slash from url
            url = url.replace(/\/$/g, "");
            console.log("New request for page: " + url);
            // Check it it's a valid URL
            if (urlRegex.test(url)) {
                // Create promise to handle page scraping
                var promise = new Promise(function (resolve, reject) {
                    request(url, function (error, response, body) {
                        if (error) {
                            reject(false);
                        }
                        else if (response.statusCode == 200) {
                            var filteredBody = body.replace(protocolRegex, 'https:');
                            resolve(filteredBody);
                        }
                    });
                });
                // On successful scrape
                promise.then(function (body) {
                    // Replace relative paths with absolute paths
                    // Replace HREFs
                    body = body.replace(/href="\//g, 'href="' + baseUrl + "/");
                    body = body.replace(/href='\//g, 'href=\'' + baseUrl + "/");
                    // Replace SRCs
                    body = body.replace(/src="\//g, 'src="' + baseUrl + "/");
                    body = body.replace(/src='\//g, 'src="' + baseUrl + "/");
                    // Replace URL(s
                    body = body.replace(/url\(\//g, 'url(' + baseUrl + '/');
                    body = body.replace(/url\('\//g, 'url(\'' + baseUrl + '/');
                    // Respond with HTML
                    res.send(body);
                    console.timeEnd('Response sent');
                }).catch(function (error) {
                    // Respond with error
                    res.send(error);
                    console.timeEnd('Response sent');
                });
            }
            else {
                // Respond with error
                res.send('ERROR');
            }
        });
        app.post('/scrape', function (req, res) {
            console.log('New SCRAPE request[' + req.body.template.name + ']: ' + req.body.url);
            var url = req.body.url, template = req.body.template;
            var fetcher = new Fetcher_1.Fetcher([url]);
            while (!fetcher.fetcherEmpty()) {
                try {
                    fetcher.fetchNext(handleParsing);
                }
                catch (e) {
                    console.log(e.message);
                }
            }
            function handleParsing(err, data) {
                if (err) {
                    console.log("Error: " + err.message);
                }
                else {
                    var parser_1 = new Parser_1.Parser(data, template).once('complete', function () {
                        console.log('Scraping done for project: ' + template.name);
                        var results = parser_1.result();
                        // Loop through template children and add data
                        for (var i = 0; i < template.children.length; ++i) {
                            // Loop through results and if name matches, add data
                            for (var j = 0; j < results._children.length; ++j) {
                                // If the names of the children are the same, assign data to template children
                                if (template.children[i].name === results._children[j]["_name"]) {
                                    template.children[i].data = results._children[j]["_data"];
                                }
                            }
                        }
                        res.send(template);
                    });
                    parser_1.parse();
                }
            }
        });
        console.log('Base Router Configured');
    }
    return BaseRouter;
}());
exports.BaseRouter = BaseRouter;
