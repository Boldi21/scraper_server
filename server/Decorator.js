"use strict";
var Meta_1 = require("./Meta");
/**
 * Decorator for Containers
 * @param className
 * @returns {(target:any)=>undefined}
 * @constructor
 */
function Container(className) {
    return function (target) {
        console.log('Adding type ' + className);
        Meta_1.Meta.types[!!className ? className : target.name] = target;
    };
}
exports.Container = Container;
